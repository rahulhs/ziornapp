export const STARS_URL =
  "https://fpdl.vimeocdn.com/vimeo-prod-skyfire-std-us/01/337/9/226685144/796795744.mp4?token=1559388554-0xd1068b6d1fdd59125e2f2ca0a0cc3fe8dcd22b76";

export const WAVES_URL =
  "https://player.vimeo.com/external/289256288.hd.mp4?s=312f6b5de3446d7e343809348f6cb5fa79afe767&amp;profile_id=175&amp;oauth2_token_id=57447761";

export const RESORT_URL =
  "https://player.vimeo.com/external/226689471.hd.mp4?s=792395d0d234688b12b258753746fdda086400cc&amp;profile_id=175&amp;oauth2_token_id=57447761";

export const NIGHT_LAPSE =
  "https://player.vimeo.com/external/303953174.hd.mp4?s=b23068f8f2ba818196a3923903214dec235eb1bf&amp;profile_id=175&amp;oauth2_token_id=57447761";

export const JOKALI =
  "https://player.vimeo.com/external/310438775.hd.mp4?s=2ffe0ba9beae4f3dee6348a0923c6a360696a03e&amp;profile_id=174&amp;oauth2_token_id=57447761";

export const TREE_REFLECTION =
  "https://player.vimeo.com/external/190380753.hd.mp4?s=947b423f39fbda0027d9aef8593103f3792d3070&amp;profile_id=119&amp;oauth2_token_id=57447761";

export const COFFEE =
  "https://player.vimeo.com/external/277521118.hd.mp4?s=81c0247dc828c14e04d6d8c38b66992526e6166b&amp;profile_id=174&amp;oauth2_token_id=57447761";
