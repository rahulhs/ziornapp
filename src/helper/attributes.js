export const ATTRIBUTES = [
  {
    name: "Temperature (°C)",
    icon: "hot",
    value: "22.5° C",
    key: "Temperature",
    unit: "°"
  },
  {
    name: "Lumens (lm)",
    icon: "lightbulb",
    value: "240 lm",
    key: "Luminance",
    unit: ""
  },
  {
    name: "Humidity (%)",
    icon: "humidity",
    value: "41%",
    key: "Relative Humidity",
    unit: ""
  },
  {
    name: "Radiation (UV)",
    icon: "filter-uv",
    value: "5 UV",
    key: "Ultraviolet",
    unit: ""
  },
  {
    name: "Occupancy (#)",
    icon: "collaboration",
    value: "0",
    key: "Employee",
    unit: ""
  }
];

export const TIMELINE_SLOTS = [
  {
    from: "09:00 AM",
    to: "10:00 AM",
    status_1: "available",
    status_2: "available",
    slots: [0, 1]
  },
  {
    from: "10:00 AM",
    to: "11:00 AM",
    status_1: "available",
    status_2: "available",
    slots: [2, 3]
  },
  {
    from: "11:00 AM",
    to: "12:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [4, 5]
  },
  {
    from: "12:00 PM",
    to: "01:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [6, 7]
  },
  {
    from: "01:00 PM",
    to: "02:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [8, 9]
  },
  {
    from: "02:00 PM",
    to: "03:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [10, 11]
  },
  {
    from: "03:00 PM",
    to: "04:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [12, 13]
  },
  {
    from: "04:00 PM",
    to: "05:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [14, 15]
  },
  {
    from: "05:00 PM",
    to: "06:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [16, 17]
  },
  {
    from: "06:00 PM",
    to: "07:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [18, 19]
  },
  {
    from: "07:00 PM",
    to: "08:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [20, 21]
  },
  {
    from: "08:00 PM",
    to: "09:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [22, 23]
  },
  {
    from: "09:00 PM",
    to: "10:00 PM",
    status_1: "available",
    status_2: "available",
    slots: [24, 25]
  }
];
