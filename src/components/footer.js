import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
  FlatList
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import moment from "moment";
import { TIMELINE_SLOTS } from "../helper/attributes";
import CustomIcon from "./CustomIcon";
import CountDown from "react-native-countdown-component";

export default class Footer extends Component {
  getStatusColor = status => {
    if (status == "available") {
      return "rgba(0, 255, 00, 1)";
    } else if (status == "booked") {
      return "rgba(255, 00, 00, 1)";
    } else if (status == "current") {
      return "#rgba(255,140,0,1)";
    }
  };

  renderTimeSlots = data => {
    return (
      <View>
        <View
          style={{
            flex: 1,
            height: hp("5"),
            width: wp("7.60"),
            paddingStart: hp("1")
          }}
        >
          <View
            style={{
              position: "absolute",
              left: 0,
              bottom: 3,
              height: hp("0.5"),
              width: wp("3.85"),
              backgroundColor: this.getStatusColor(data.item.status_1)
            }}
          />
          <View
            style={{
              position: "absolute",
              right: 0,
              bottom: 3,
              height: hp("0.5"),
              width: wp("3.85"),
              backgroundColor: this.getStatusColor(data.item.status_2)
            }}
          />
          {data.index == TIMELINE_SLOTS.length - 1 ? (
            <View
              style={{
                position: "absolute",
                right: 0,
                bottom: 3,
                backgroundColor: "rgba(255,255,255,0.5)",
                width: 1,
                height: hp("5")
              }}
            />
          ) : null}
          <View
            style={{
              position: "absolute",
              left: 0,
              bottom: 3,
              backgroundColor: "rgba(255,255,255,0.5)",
              width: 1,
              height: hp("5")
            }}
          />
          <Text style={{ fontSize: hp("2"), color: "white" }}>
            {data.item.from}
          </Text>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
            alignItems: "flex-end"
          }}
        >
          {this.props.showTimer && (
            <View
              style={{
                alignItems: "flex-start",
                alignContent: "center",
                flex: 1,
                marginStart: 20,
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  color: "rgba(255, 255, 255, 0.6)",
                  fontSize: hp("3%"),
                  marginTop: hp("0%"),
                  textAlign: "right",
                  marginStart: hp("1"),
                  marginBottom: hp("1"),
                  fontFamily: "MP-Regular"
                }}
              >
                Meeting ends in
              </Text>
              <CountDown
                size={20}
                until={this.props.countDownInSeconds}
                onFinish={() => this.props.callback()}
                digitStyle={{
                  backgroundColor: "transparent",
                  borderWidth: 1,
                  borderColor: "rgba(255,255,255,0.5)",
                  alignSelf: "flex-start",
                  alignItems: "center"
                }}
                digitTxtStyle={{
                  color: "rgba(255,255,255,1)",
                  fontWeight: "normal",
                  fontFamily: "MP-Regular"
                }}
                timeLabelStyle={{
                  color: "red",
                  fontWeight: "bold",
                  fontFamily: "MP-Regular"
                }}
                separatorStyle={{ color: "rgba(255,255,255,0.5)" }}
                timeToShow={["H", "M", "S"]}
                timeLabels={{ m: null, s: null }}
                showSeparator
              />
            </View>
          )}
          <View
            style={{
              margin: hp("7"),
              flex: 1,
              alignItems: "flex-end",
              alignSelf: "flex-end"
            }}
          >
            <Text
              style={{
                color: "white",
                fontSize: hp("4%"),
                fontFamily: "MP-Regular"
              }}
            >
              {moment().format("hh:mm A")}
            </Text>
            <Text
              style={{
                color: "white",
                fontSize: hp("2.5%"),
                fontFamily: "MP-Regular"
              }}
            >
              MEETING ROOM BOOKING STATUS
            </Text>
            <Text
              style={{
                color: "white",
                textAlign: "right",
                fontSize: hp("5%"),
                lineHeight: hp("6"),
                fontFamily: "MP-Regular"
              }}
            >
              {this.props.status}
            </Text>
            <Text
              style={{
                color: "rgba(255, 255, 255, 0.6)",
                fontSize: hp("3%"),
                marginTop: hp("2.5%"),
                textAlign: "right",

                fontFamily: "MP-Regular"
              }}
            >
              {this.props.bookingName}
            </Text>
            <Text
              style={{
                color: "white",
                fontSize: hp("3%"),
                fontFamily: "MP-Regular",
                color: "rgba(255, 255, 255, 0.6)"
              }}
            >
              {this.props.bookingTime}
            </Text>
          </View>
        </View>
        <View>
          {/* TimeSLot */}
          <FlatList
            contentContainerStyle={{
              marginTop: hp("0"),
              marginStart: hp("1"),
              marginEnd: hp("2")
            }}
            data={this.props.timeslots}
            numColumns={1}
            renderItem={data => {
              return this.renderTimeSlots(data);
            }}
            horizontal={true}
            keyExtractor={item => {
              return item.from;
            }}
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }
}
