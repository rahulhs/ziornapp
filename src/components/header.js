import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity, Animated } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import CustomIcon from "./CustomIcon";

export default class Header extends Component {
  render() {
    return (
      <View>
        <View
          style={{
            position: "absolute",
            top: 0,
            width: wp("100%"),
            height: hp("25%"),
            backgroundColor: "black",
            opacity: 0.3
          }}
        />

        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View>
            <View
              style={{
                alignContents: "flex-start",
                flex: 1,
                marginTop: hp("-3"),
                marginBottom: hp("3"),
                alignSelf: "flex-start"
              }}
            >
              <CustomIcon
                name="io_logo"
                size={hp(10)}
                color="white"
                style={{
                  marginTop: hp("4"),
                  marginStart: hp("6")
                }}
              />
            </View>
            <Text
              style={{
                fontFamily: "MP-Regular",
                color: "white",
                fontSize: hp("10%"),
                marginTop: hp("10%"),
                marginStart: hp("7.5%")
              }}
            >
              {this.props.title}
            </Text>
          </View>
          <View style={{ marginEnd: hp("7"), marginTop: hp("3") }}>
            <Text
              style={{
                color: "white",
                fontSize: hp("4%"),
                fontFamily: "MP-Regular"
              }}
            >
              {this.props.date}
            </Text>
            <Text
              style={{
                color: "white",
                fontSize: hp("2%"),
                fontFamily: "MP-Regular"
              }}
            >
              {this.props.location}
            </Text>
            <View style={{ flexDirection: "row", marginTop: hp("3%") }}>
              <Text
                style={{
                  color: "white",
                  fontSize: hp("5%"),
                  fontFamily: "MP-Regular"
                }}
              >
                {this.props.cityTemperature}
              </Text>
              <Image
                style={{
                  height: hp("5%"),
                  width: wp("5%"),
                  marginStart: 10,
                  marginTop: 5
                }}
                source={{
                  uri: this.props.imageUrl
                }}
              />
            </View>
            <Text
              style={{
                color: "white",
                fontSize: hp("1.8%"),
                fontFamily: "MP-Regular"
              }}
            >
              {this.props.tempDesc}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
