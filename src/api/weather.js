//https://samples.openweathermap.org/data/2.5/weather?q=Bangalore&appid=b6907d289e10d714a6e88b30761fae22

import fetchAPI from "../utils/fetch";

export default function getBangaloreWeather() {
  // console.log("PING API 1", pingurl);

  const request = {
    url:
      "https://samples.openweathermap.org/data/2.5/weather?q=Bangalore&appid=b6907d289e10d714a6e88b30761fae22",
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  };
  // console.log("GET PROFILE API REQUEST>", request);
  return fetchAPI(request, true);
}
