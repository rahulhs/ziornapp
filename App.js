/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  Text,
  StatusBar,
  View,
  ImageBackground,
  StyleSheet,
  PermissionsAndroid,
  FlatList
} from "react-native";

import AsyncStorage from "@react-native-community/async-storage";
import RNSoundLevel from "react-native-sound-level";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";

var running_on_android_tv = Platform.isTV;
import RNFetchBlob from "react-native-fetch-blob";
import Video from "react-native-video";
import TimeSlotUtil from "./src/utils/timeSlotUtil";

import Header from "./src/components/header";
import Footer from "./src/components/footer";
import CustomIcon from "./src/components/CustomIcon";
import { ATTRIBUTES } from "./src/helper/attributes";
import firebase from "react-native-firebase";
import moment from "moment";
import { TIMELINE_SLOTS } from "./src/helper/attributes";
import getBangaloreWeather from "./src/api/accuWeather";

type Props = {};
export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      responseData: {},
      date: moment().format("DD MMM, YYYY"),
      cityTemp: "",
      cityTempDesc: "",
      cityTempImage:
        "http://dataservice.accuweather.com/developers/Media/Default/WeatherIcons/03-s.png",
      personCount: 0,
      meetingList: [],
      timelineSlots: JSON.parse(JSON.stringify(TIMELINE_SLOTS)),
      isFileSaved: false,
      localVideoUri: "",
      soundLevel: 0,
      backVideoUrl:
        "https://gcs-vimeo.akamaized.net/exp=1559390891~acl=%2A%2F1094756916.mp4%2A~hmac=4692a6dca173e4089cd8e9ccc9518610e46c8c8a7a4a1ba25fc8f183cd7e11cc/vimeo-prod-skyfire-std-us/01/2851/11/289258217/1094756916.mp4",
      countDownInSeconds: 0,
      showTimer: false,
      bookedBy: "N/A",
      nextMeetingTime: "",
      currentMeetingTime: "",
      currentHost: "",
      meetingRoomStatus: "Available\nFor Booking",
      meetingInProgress: false
    };
    console.log("DATE>>", this.state.date);

    firebase
      .auth()
      .signInAnonymously()
      .then(() => {
        console.log("LOGINSUCCESS");
        // this.listenForSensorData();
      })
      .catch(err => {
        console.log('errrrrr!')
        console.log(err);
      });
  }

  componentDidMount() {
    this.getWeather();
    this.timer1 = setInterval(() => this.getWeather(), 1000 * 60 * 10);
    this.timer2 = setInterval(() => this.updateTimelineSlots(), 1000 * 60);
    // this.requestMicroPhonePermission();
  }

  componentWillUnmount() {
    // RNSoundLevel.stop();
  }

  requestMicroPhonePermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        {
          title: "Interaction One Needs Audio Record permission",
          message:
            "IO uses the audio listened to calculate the sound decibel in the room, no data is saved/uploaded to the server.",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        RNSoundLevel.start();
        RNSoundLevel.onNewFrame = data => {
          // see "Returned data" section below
          console.log("Sound level info", data);
          this.setState({
            soundLevel: data.value
          });
        };
      } else {
        console.log("Permission denied");
        this.requestMicroPhonePermission();
      }
    } catch (err) {
      console.warn(err);
    }
  };

  getWeather = () => {
    getBangaloreWeather()
      .then(data => data.json())
      .then(response => {
        console.log(JSON.stringify(response));
        console.log("CITY TEMP", response[0].Temperature.Metric.Value);
        let baseUrl =
          "http://dataservice.accuweather.com/developers/Media/Default/WeatherIcons/";
        let weatherIcon = 0;
        if (response[0].WeatherIcon < 10) {
          weatherIcon = "0" + response[0].WeatherIcon;
        } else {
          weatherIcon = response[0].WeatherIcon;
        }
        let imageUrl = baseUrl + weatherIcon + "-s.png";
        console.log("IMAGE URL>", imageUrl);
        this.setState({
          cityTemp: response[0].Temperature.Metric.Value + "° C",
          cityTempImage: imageUrl,
          cityTempDesc: response[0].WeatherText
        });
      });
  };

//   listenForSensorData = () => {
//     this.getRef()
//       .child("546-789-657")
//       .child("multisensor")
//       .on("value", snap => {
//         console.log("RESPONSE>>", JSON.stringify(snap.val()));
//         this.setState({
//           responseData: snap.val()
//         });
//       });
//     this.getRef()
//       .child("motionsensor")
//       .on("value", snap => {
//         console.log("RESPONSE>>", JSON.stringify(snap.val()));
//         this.setState({
//           personCount: snap.val()["motiondetected"]
//         });
//       });
//     this.getRef()
//       .child("546-789-657")
//       .child("meetings")
//       .on("value", snap => {
//         console.log("RESPONSE>>", JSON.stringify(snap.val()));
//         let meetingsArray = [];
//         //console.log(JSON.stringify(snap.val()));
//         try {
//           Object.keys(snap.val()).forEach(function(key) {
//             meetingsArray.push(snap.val()[key]);
//           });
//         } catch (e) {}
//         if (meetingsArray.length == 1) {
//           let bookedBy = meetingsArray[0].creator.email
//             .split("@")[0]
//             .replace(".", " ");
//           //bookedBy = bookedBy.charAt(0).toUpperCase() + bookedBy.slice(1);
//           bookedBy = bookedBy.toUpperCase();
//           let startTimeOr = meetingsArray[0].start.dateTime;
//           let endTimeOr = meetingsArray[0].end.dateTime;

//           let dateofMeetingUnformated = startTimeOr.split("T")[0];
//           //TODO:Check current date and then only perform

//           let startTime = startTimeOr.split("T")[1].split("+")[0];
//           let endTime = endTimeOr.split("T")[1].split("+")[0];

//           this.setState({
//             meetingList: meetingsArray
//           });
//         } else {
//           this.setState({
//             meetingList: meetingsArray,
//             bookedBy: "N/A",
//             nextMeetingTime: ""
//           });
//         }

//         this.updateTimelineSlots();
//       });
//     this.getRef()
//       .child("546-789-657")
//       .child("backgroundvideo")
//       .on("value", snap => {
//         console.log("BACK>>", snap.val());
//         this.setState({
//           backVideoUrl: snap.val()
//         });

//         this.downloadAndPlayVideo(snap.val());
//       });
//   };
  downloadAndPlayVideo = videoUrl => {
    let dirs = RNFetchBlob.fs.dirs;
    let videoPath = dirs.DocumentDir + "/backvideo.mp4";
    RNFetchBlob.fs
      .exists(videoPath)
      .then(exist => {
        console.log(`file ${exist ? "" : "not"} exists`);
        return exist;
      })
      .then(videoExist => {
        let obj = {
          videoExist: videoExist,
          url: false
        };
        return AsyncStorage.getItem("backvideourl").then(url => {
          if (url) {
            obj.url = url;
            return obj;
          } else {
            obj.url = null;
            return obj;
          }
        });
      })
      .then(resultObj => {
        if (resultObj.videoExist && resultObj.url == videoUrl) {
          this.setState({
            localVideoUri: "file://" + res.path(),
            isFileSaved: true
          });
          console.log("Video exists so not downloading again");
        } else {
          RNFetchBlob.fs.unlink(videoPath).then(() => {
            console.log("UNLINKED THE VIDEO");
          });
          if (!resultObj.videoExist) {
            console.log("Video Doesn't exist so downloading");
          }
          if (resultObj.url != videoUrl) {
            console.log("Video has been changed so downloading");
          }
          RNFetchBlob.config({
            // add this option that makes response data to be stored as a file,
            // this is much more performant.
            fileCache: true,
            appendExt: "mp4",
            session: "foo",
            path: videoPath
          })
            .fetch("GET", videoUrl, {
              //some headers ..
            })
            .then(res => {
              // the temp file path
              console.log("The file saved to ", "file://", res.path());
              AsyncStorage.setItem("backvideourl", videoUrl);
              this.setState({
                localVideoUri: "file://" + res.path(),
                isFileSaved: true
              });
            });
        }
      })
      .catch(() => {});
  };

  updateTimelineSlots = () => {
    let tempSlots = JSON.parse(JSON.stringify(TIMELINE_SLOTS));
    let tempMeetingList = [...this.state.meetingList];
    // console.log("MEETING LIST STATE>>", JSON.stringify(TIMELINE_SLOTS));
    //Updating current time slot
    var current_time = new moment().format("hh:mm A");
    console.log("Current time>", current_time);
    tempSlots.map(slot => {
      if (
        slot.from.split(":")[0] == current_time.split(":")[0] &&
        slot.from.split(" ")[1] == current_time.split(" ")[1]
      ) {
        if (current_time.split(":")[1].split(" ")[0] >= 30) {
          slot.status_2 = "current";
        } else {
          slot.status_1 = "current";
        }
      }
    });

    tempMeetingList.forEach(meeting => {
      let meetingStartTime = this.formatTime(
        meeting.start.dateTime.split("T")[1].split("+")[0]
      );
      let meetingEndTime = this.formatTime(
        meeting.end.dateTime.split("T")[1].split("+")[0]
      );
      let a = "06:30 PM";
      let b = "06:04 PM";
      a = meetingStartTime;
      b = current_time;
      let c = meetingEndTime;
      meetingStartTime = moment(a, "hh:mm A");
      meetingEndTime = moment(c, "hh:mm A");

      var currentMarker = moment(b, "hh:mm A");

      var ds = moment.duration(meetingStartTime.diff(currentMarker));
      var st = ds.asHours();

      var de = moment.duration(meetingEndTime.diff(currentMarker));
      var en = de.asHours();
      if (st < 0 && en > 0) {
        let host = meeting.creator.email
          .split("@")[0]
          .replace(".", " ")
          .toUpperCase();
        this.setState({
          meetingInProgress: true,
          countDownInSeconds: de.asSeconds(),
          showTimer: true,
          currentHost: host,
          meetingRoomStatus: "Meeting In Progress\n by " + host
        });
        if (tempMeetingList.length > 1) {
          let bookedBy = tempMeetingList[1].creator.email
            .split("@")[0]
            .replace(".", " ");
          let startTimeOr = tempMeetingList[1].start.dateTime;
          let endTimeOr = tempMeetingList[1].end.dateTime;

          let startTime1 = startTimeOr.split("T")[1].split("+")[0];
          let endTime1 = endTimeOr.split("T")[1].split("+")[0];
          this.setState({
            bookedBy: "Up Next : " + bookedBy.toUpperCase(),
            nextMeetingTime:
              this.formatTime(startTime1) + " - " + this.formatTime(endTime1)
          });
        }
      }

      let meetingDate = moment(meeting.start.dateTime.split("T")[0]).format(
        "DD-MM-YYYY"
      ); //2019-05-30 = 30-05-2019
      //console.log("MEETING DATE>>");
      if (meetingDate == moment().format("DD-MM-YYYY")) {
        let slotsToBeFilledBlack = TimeSlotUtil.findSlotsBtwStartAndEnd(
          meetingStartTime,
          meetingEndTime
        );
        tempSlots.map(timeblock => {
          slotsToBeFilledBlack.forEach(svalue => {
            let index = timeblock.slots.indexOf(svalue);
            if (index == 0) {
              timeblock.status_1 = "booked";
              // console.log("FOUND>>>>>0", timeblock);
            } else if (index == 1) {
              timeblock.status_2 = "booked";
              // console.log("FOUND>>>>>1", timeblock);
            }
          });
        });
      }
    });
    if (!this.state.meetingInProgress && tempMeetingList.length > 0) {
      let bookedBy = tempMeetingList[0].creator.email
        .split("@")[0]
        .replace(".", " ");
      let startTimeOr = tempMeetingList[0].start.dateTime;
      let endTimeOr = tempMeetingList[0].end.dateTime;

      let startTime1 = startTimeOr.split("T")[1].split("+")[0];
      let endTime1 = endTimeOr.split("T")[1].split("+")[0];
      this.setState({
        bookedBy: "Up Next : " + bookedBy.toUpperCase(),
        nextMeetingTime:
          this.formatTime(startTime1) + " - " + this.formatTime(endTime1)
      });
    } else if (!this.state.meetingInProgress) {
      this.setState({
        bookedBy: "N/A",
        showTimer: false,
        meetingRoomStatus: " Available\nFor Booking"
      });
    }
    if (this.state.countDownInSeconds == 0) {
      this.setState({
        currentHost: "",
        showTimer: false,
        meetingRoomStatus: " Available\nFor Booking"
      });
    }
    //console.log("FINAL OUTPUT>>>", JSON.stringify(tempSlots));
    this.setState({
      timelineSlots: tempSlots
    });
  };

  findSlotsBtwStartAndEnd = (startTime, endTime) => {
    let startMarker = "09:00 AM";
    let endMarker = "10:00 PM";
  };

  formatTime = time => {
    var formatted = moment(time, "HH:mm:ss").format("hh:mm A");
    return formatted;
  };

  getRef() {
    return firebase.database().ref();
  }

  renderAttributes = data => {
    return (
      // 97aeae,
      <View
        style={{
          flex: 1,
          height: hp("18"),
          width: wp("15"),
          borderRightColor: "rgba(255,255,255,0.1)",
          borderRightWidth: data.index == 5 ? 0 : 1,
          paddingStart: wp("3"),
          paddingEnd: wp("3"),
          marginTop: hp("5")
        }}
      >
        <CustomIcon
          name={data.item.icon}
          size={hp(4)}
          style={{
            marginTop: hp(1),
            color: "white",
            alignSelf: "center"
          }}
        />
        <Text
          style={{
            color: "white",
            marginTop: hp("1"),
            fontSize: hp("1.8"),
            textAlign: "center",
            fontFamily: "MP-Regular"
          }}
        >
          {data.item.name.toUpperCase()}
        </Text>
        {data.item.key == "Employee" ? (
          <Text
            style={{
              color: "white",
              fontSize: hp("5"),
              textAlign: "center",
              marginTop: hp("2.5"),
              fontFamily: "MP-Regular"
            }}
          >
            {this.state.personCount}
          </Text>
        ) : data.item.key == "Sound" ? (
          <Text
            style={{
              color: "white",
              fontSize: hp("5"),
              textAlign: "center",
              marginTop: hp("2.5"),
              fontFamily: "MP-Regular"
            }}
          >
            {this.state.soundLevel}
          </Text>
        ) : (
          <Text
            style={{
              color: "white",
              fontSize: hp("5"),
              textAlign: "center",
              marginTop: hp("2"),
              fontFamily: "MP-Regular"
            }}
          >
            {this.state.responseData[data.item.key] + data.item.unit}
          </Text>
        )}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden />
        {/* <ImageBackground
          style={{ width: wp("100%"), height: hp("100%") }}
          source={require("./src/images/dashboard_bgd.png")}
        > */}

        <Video
          repeat
          source={
            this.state.isFileSaved
              ? {
                  uri: this.state.localVideoUri
                }
              : {
                  uri: this.state.backVideoUrl
                }
          }
          ref={player => {
            this.player = player;
          }}
          resizeMode="cover"
          style={[StyleSheet.absoluteFill]}
        />
        <View
          style={[
            StyleSheet.absoluteFill,
            { backgroundColor: "rgba(0,0,0,0.4)" }
          ]}
        />

        <Header
          title="Board Room"
          date={this.state.date}
          location="Bangalore, India"
          cityTemperature={this.state.cityTemp}
          tempDesc={this.state.cityTempDesc}
          imageUrl={this.state.cityTempImage}
        />
        {this.state.responseData.Temperature && (
          <FlatList
            contentContainerStyle={{
              marginTop: hp("7"),
              marginStart: hp("5")
            }}
            data={ATTRIBUTES}
            numColumns={1}
            renderItem={data => {
              return this.renderAttributes(data);
            }}
            horizontal={true}
            keyExtractor={item => {
              return item.name;
            }}
            showsHorizontalScrollIndicator={false}
          />
        )}

        <View style={{ position: "absolute", bottom: 0, end: 0, left: 0 }}>
          <Footer
            callback={meetingEnded => {
              this.setState({
                showTimer: false,
                countDownInSeconds: 0
              });
            }}
            countDownInSeconds={this.state.countDownInSeconds}
            showTimer={this.state.showTimer}
            status={this.state.meetingRoomStatus}
            bookingName={this.state.bookedBy}
            bookingTime={this.state.nextMeetingTime}
            timeslots={this.state.timelineSlots}
          />
        </View>
        {/* </ImageBackground> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  textWrapper: {
    height: hp("70%"), // 70% of height device screen
    width: wp("80%") // 80% of width device screen
  },
  myText: {
    color: "white",
    fontSize: hp("5%") // End result looks like the provided UI mockup
  }
});
